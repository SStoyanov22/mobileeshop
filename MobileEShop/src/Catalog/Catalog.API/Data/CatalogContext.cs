﻿namespace Catalog.API.Data
{
    using Catalog.API.Data.Interfaces;
    using Catalog.API.Entities;
    using Catalog.API.Settings;
    using MongoDB.Driver;

    public class CatalogContext : ICatalogContext
    {
        public CatalogContext(ICatalogDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            Products = database.GetCollection<Product>(settings.ColletionName);
                CatalogContextSeed.SeedData(Products);
        }
        public IMongoCollection<Product> Products { get; }
    }
}
