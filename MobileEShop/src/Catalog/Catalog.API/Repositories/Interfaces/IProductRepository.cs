﻿namespace Catalog.API.Repositories.Interfaces
{
    using Catalog.API.Entities;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IProductRepository
    {
        //GET
        Task<IEnumerable<Product>> GetProducts();
        //GET
        Task<Product> GetProduct(string id);
        //GET
        Task<IEnumerable<Product>> GetProductByName(string name);
        //GET
        Task<IEnumerable<Product>> GetProductByCategory(string categoryName);
        //POST
        Task Create(Product product);
        //PUT
        Task<bool> Update(Product product);
        //DELETE
        Task<bool> Delete(string id);


    }
}
