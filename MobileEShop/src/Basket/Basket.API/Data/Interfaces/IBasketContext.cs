﻿namespace Basket.API.Data.Interfaces
{
    using StackExchange.Redis;

    public interface IBasketContext
    {
        IDatabase Redis { get; }
    }
}
