﻿namespace Basket.API.Data
{
    using Basket.API.Data.Interfaces;
    using StackExchange.Redis;
    using System;

    public class BasketContext : IBasketContext
    {
        private readonly ConnectionMultiplexer _redisConnection;
         
        public BasketContext(ConnectionMultiplexer redisConnection)
        {
            this._redisConnection = redisConnection;
            Redis = redisConnection.GetDatabase();
        }
        public IDatabase Redis { get; }
    }
}
